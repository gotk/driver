package rdbc

import (
	"context"
	"sync"

	"github.com/go-redis/redis/v8"
)

// stdMap 全局redis连接句柄池
// key string 是一个实例的名字
// value RedisOperator 是连接句柄
var stdRedisPool RedisOperator

type RedisPool struct {
	Pool sync.Map
}

// RedisOperator redis 多实例操作接口
type RedisOperator interface {
	// Create 初始化一个连接 失败时将panic
	Create(key string, opt *Config)
	// Delete 移除一个连接
	Delete(key string)
	// GetConn 获取一个Redis连接
	GetConn(key string) (redis.UniversalClient, bool)
}

func GetRedisPool() RedisOperator {
	if stdRedisPool == nil {
		stdRedisPool = &RedisPool{}
	}
	return stdRedisPool
}

// Create 初始化一个连接 失败时将panic
func (rp *RedisPool) Create(key string, opt *Config) {
	client := redis.NewUniversalClient(&redis.UniversalOptions{
		Addrs:        []string{opt.Address},
		DB:           opt.Db,
		Password:     opt.Password,
		PoolSize:     opt.MaxActive,
		MinIdleConns: opt.MaxIdle,
	})
	err := client.Ping(context.Background()).Err()
	if err != nil {
		panic(err)
	}
	if _, isLoad := rp.Pool.LoadOrStore(key, client); isLoad {
		panic("redis connect key exist")
	}
}

// Delete 移除一个连接
func (rp *RedisPool) Delete(key string) {
	rp.Pool.Delete(key)
}

// GetConn 获取一个Redis连接
func (rp *RedisPool) GetConn(key string) (redis.UniversalClient, bool) {
	v, exist := rp.Pool.Load(key)
	if !exist {
		return nil, false
	}
	return v.(redis.UniversalClient), true
}
