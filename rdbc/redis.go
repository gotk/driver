package rdbc

import (
	"context"
	"fmt"

	"github.com/go-redis/redis/v8"
)

var (
	std redis.UniversalClient
)

// GetConn 获取一个redis连接句柄
func GetConn() redis.UniversalClient {
	if std == nil {
		panic("redis conn not initialized")
	}
	return std
}

// NewClient 新建一个Redis全局连接池，请注意传入 被初始化一次过后 再次初始化将不会发生任何事情
func NewClient(opt *redis.UniversalOptions) {
	// 已经存在了 不需要再次初始化
	if std != nil {
		return
	}
	client, err := NewClientSingle(opt)
	if err != nil {
		panic(err)
	}
	if client == nil {
		panic("connect to redis service failed")
	}
	std = client
}

// NewClientSingle 新建一个Redis连接池 请保持该连接 否则用完就释放了
func NewClientSingle(opt *redis.UniversalOptions) (redis.UniversalClient, error) {
	client := redis.NewUniversalClient(opt)
	if client == nil {
		return nil, fmt.Errorf("client is nil")
	}
	err := client.Ping(context.Background()).Err()
	if err != nil {
		return nil, err
	}
	return client, nil
}

// AddHook 添加钩子
func AddHook(hook redis.Hook) {
	if std == nil {
		panic("redis not initialized")
	}
	std.AddHook(hook)
}
