package rdbc

import "time"

type Config struct {
	Network        string        `yaml:"network"`
	Address        string        `yaml:"address"`
	ConnectTimeout time.Duration `yaml:"connect-timeout"`
	// the database to select when dialing a connection.
	Db int `yaml:"db"`
	//  the password to use when connecting to the Redis server.
	Password string `yaml:"password"`
	// the timeout for reading a single command reply.
	ReadTimeout time.Duration `yaml:"read-timeout"`
	// the timeout for writing a single command.
	WriteTimeout time.Duration `yaml:"write-timeout"`
	// the keep-alive period for TCP connections to the Redis server
	// when no DialNetDial option is specified.
	// If zero, keep-alives are not enabled. If no DialKeepAlive option is specified then
	// the default of 5 minutes is used to ensure that half-closed TCP sessions are detected.
	KeepAlive time.Duration `yaml:"keep-alive"`
	// Maximum number of idle connections in the pool.
	MaxIdle int `yaml:"max-idle"`
	// Maximum number of connections allocated by the pool at a given time.
	// When zero, there is no limit on the number of connections in the pool.
	MaxActive int `yaml:"max-active"`
	// Close connections after remaining idle for this duration. If the value
	// is zero, then idle connections are not closed. Applications should set
	// the timeout to a value less than the server's timeout.
	IdleTimeout time.Duration `yaml:"idle-timeout"`
	// Close connections older than this duration. If the value is zero, then
	// the pool does not close connections based on age.
	// WaitTimeout time.Duration `yaml:"wait-timeout"`
	// If WaitTimeout is not set, then Wait effects.
	// if Wait is set true, then wait until ctx timeout, or default flase and return directly.
	Wait bool `yaml:"wait"`
	EnableCache bool `yaml:"enable-cache"`
}
