module gitlab.com/gotk/driver

go 1.16

require gitlab.com/gotk/mdbc v0.0.0-20220223085945-32b899744110

require (
	github.com/go-redis/redis/v8 v8.11.4
	go.mongodb.org/mongo-driver v1.8.3
)
