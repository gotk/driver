package driver

import (
	"github.com/go-redis/redis/v8"
	"gitlab.com/gotk/driver/rdbc"
	"gitlab.com/gotk/mdbc"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

type Config struct {
	MongoConf *mdbc.Config
	RedisConf *rdbc.Config
}

// 全局配置
var stdDriver *Driver

type Driver struct {
	*Config
}

// NewDriver 初始化一个driver控制器
func NewDriver(cfg *Config) *Driver {
	stdDriver = &Driver{
		Config: cfg,
	}

	return stdDriver
}

// GetDriver 获取驱动器
func GetDriver() *Driver {
	return stdDriver
}

// StartMongoDB 开启全局mdbc 传入mongodb所需要的参数 目前支持以下配置
// *readpref.ReadPref 指定读模式
func (d *Driver) StartMongoDB(opts ...interface{}) *Driver {
	if d.MongoConf != nil {
		for _, opt := range opts {
			switch v := opt.(type) {
			case *readpref.ReadPref:
				d.MongoConf.ReadPreference = v
			}
		}
		d.MongoConf.RegistryBuilder = mdbc.RegisterTimestampCodec(nil)
		client, err := mdbc.ConnInit(d.MongoConf)
		if err != nil {
			panic(err)
		}
		mdbc.Init(client).InitDB(client.Database(d.MongoConf.DBName))
	}
	return d
}

// StartRedis 开启全局redis
func (d *Driver) StartRedis() *Driver {
	if d.RedisConf != nil {
		rdbc.NewClient(&redis.UniversalOptions{
			Addrs:        []string{d.RedisConf.Address},
			DB:           d.RedisConf.Db,
			PoolSize:     d.RedisConf.MaxActive,
			MinIdleConns: d.RedisConf.MaxIdle,
			Password:     d.RedisConf.Password,
		})
	}
	return d
}
